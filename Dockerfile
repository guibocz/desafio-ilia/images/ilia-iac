FROM alpine:edge    

RUN echo "==> Update Alpine repo <=="  && \
    apk --no-cache update && \
    \
    \
    echo "==> Installing dependencies <=="  && \
    apk add --no-cache sudo curl wget unzip && \
    \
    \
    echo "==> Install aws-iam-authenticator <==" && \
    wget https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.5.7/aws-iam-authenticator_0.5.7_linux_amd64 && \
    mv aws-iam-authenticator_0.5.7_linux_amd64 /usr/local/bin/aws-iam-authenticator && \
    chmod +x /usr/local/bin/aws-iam-authenticator && \
    \
    \
    echo "==> Install Kubectl <==" && \
    curl -o kubectl https://storage.googleapis.com/kubernetes-release/release/v1.23.5/bin/linux/amd64/kubectl && \
    install kubectl /usr/local/bin/ && \
    rm kubectl && \
    \
    \
    echo "==> Create .kube path <=="  && \
    mkdir -p /root/.kube/ && \
    \
    \
    echo "==> Install Terraform <==" && \
    wget https://releases.hashicorp.com/terraform/1.1.8/terraform_1.1.8_linux_amd64.zip && \
    unzip terraform_1.1.8_linux_amd64.zip && rm terraform_1.1.8_linux_amd64.zip && \
    mv terraform /usr/bin/terraform && \
    \
    \
    echo "==> Removing package list <=="  && \
    rm -rf /var/cache/apk/*              

WORKDIR /root

USER root