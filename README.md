# ilia-iac

## Purpose

That image was created to be used by the projects under [desafio-ilia](https://gitlab.com/guibocz/desafio-ilia)

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name              | Value                              |
|-------------------|------------------------------------|
| DOCKER_USER       | user to the docker repository used |
| DOCKER_PASSWORD   | password to the user used          |
| DOCKER_URL        | url to the docker repository used  |
| DOCKER_REPOSITORY | name of the repository path        |